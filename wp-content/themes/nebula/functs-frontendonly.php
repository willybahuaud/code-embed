<?php
/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SOMMAIRE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 1. INITIALIZATION DES SCRIPTS
// 2. COMPATIBILITES HTML5
// 3. AJOUTER REL ET CLASS A GALERIE
// 4. CUSTOMISER LA PAGE DE CONNEXION
// 5. CAROUSEL
// 6. MODULES METEO
// 7. TYPES DE WALKERS
// 8. AFFICHAGE  DU CAROUSEL A LA UNE 
// 9. FONCTIONS MULTILANGUES
//10. FIL D'ARIANE
//11. CUSTOMISATION DE L'EXCERPT
//12. FONCTIONS PROCÉDURALES
*//**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INITIALIZATION DES SCRIPTS ↓
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
add_action('wp_enqueue_scripts', 'my_scripts_method');
function my_scripts_method() {
	//jQuery
	wp_enqueue_script('jquery');
	
		// -> modernizr
	wp_deregister_script('modernizr');
		wp_register_script('modernizr', get_template_directory_uri() .'/js/modernizr3.js', false, '', true);
	wp_enqueue_script('modernizr');
	// -> html5shiv
	wp_deregister_script('html5shiv');
		wp_register_script('html5shiv', get_template_directory_uri() .'/js/html5shiv.js', false , '' ,true);
	wp_enqueue_script('html5shiv');

	wp_register_script('myjs',
		get_template_directory_uri() . '/js/script.js',
		array( 'jquery' ),
		'1.0', true );
	wp_enqueue_script('myjs');

	wp_register_style( 'style', get_stylesheet_uri(), false, '0.9', 'all' );
	wp_enqueue_style( 'style' );
}



/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 2. COMPATIBILITES HTML5 ↓
// Par défaut, on utilise HTML5 SHIV mais possibilité de switcher avec Modernizr en commentant et décommentant les add_action respectifs
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
add_action('wp_head', 'add_ie_html5_shim');
function add_ie_html5_shim () {
	echo '<!--[if lt IE 9]>';
	echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
	echo '<![endif]-->';
}

/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 4. CUSTOMISER LA PAGE DE CONNEXION ↓
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
// Fonction qui insere le lien vers le css qui surchargera celui d'origine  
// add_action('login_head', 'custom_login_css');  
function custom_login_css(){  
	echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/design/style-login.css">'; 
} 

// Filtre qui permet de changer l'url du logo  
// add_filter('login_headerurl', 'custom_url_login');
function custom_url_login()  
{  
	return 'http://wabeo.fr'; // On retourne l'index du site  
}  

// Filtre qui permet de changer l'attribut title du logo  
 // add_filter('login_headertitle', 'custom_title_login');
function custom_title_login($message)  
{  
	return 'Thème by Wabeo'; // On retourne la description du site 
} 

/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 5. CAROUSEL ↓
// lié au custom post type et meta box
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

function carouselSF($type_sortie = 'default', $size = "large", $linkclass = '', $before ='', $after ='', $echo = true){
	/**
	* @param $type_sortie : façon dont on veut que les images du diaporama soient affichées (default | liste | liens | separate-list)
	* @param $size : taille des images souhaitées en sortie
	* @param $linkclass : class des liens (pour ajouter class pirobox)
	* @param $before : balise ouvrante du diaporama
	* @param $after : balise fermante du diaporama
	* @param $echo : affiche le résultat, ou le retourne juste
	*/
	include_once('class/class-w3p-image-from-object.php');	// Ma classe
	$img_from_obj = new w3p_image_from_object();

	wp_reset_query();
	global $post;
	$ca = get_post_meta($post->ID,'_liaison_slideshow',true);
	if($ca!=FALSE){
		$list_images = list_my_images_slots();
		$img_ids = array();
		foreach($list_images as $a){
			if($i = get_post_meta($ca,$a,true))
				$img_ids[] = $i;
		}
		// $img_ids = array( 22, 45, 7, 31, 139);	// IDs des photos
		$imgs = $img_from_obj->get_results($img_ids);
		 
		$r = $before;
		switch($type_sortie){

			case 'liste':
				// SOUS FORME DE LISTE
				$r .= '<ul>';
				foreach($imgs as $img) {
					$big_src = reset( $img_from_obj->wp_get_attachment_image_src( $img, 'large' ));	// Source de l'image grand format pour une lightbox
					$img_title = trim( strip_tags( $img->post_title ) );				// Attribut title du lien
					$r .= '<a'.($img_title ? ' title="'.$img_title.'"' : '').' href="'.$big_src.'" class="'.$class.'">';
					$r .= $img_from_obj->wp_get_attachment_image($img, $size);
					$r .= '</a>';
				}
				$r .= '</ul>';
				break;

			case 'liens':
				// SOUS FORME DE LISTE
				foreach($imgs as $img) {
					$big_src = reset( $img_from_obj->wp_get_attachment_image_src( $img, 'large' ));	// Source de l'image grand format pour une lightbox
					$img_title = trim( strip_tags( $img->post_title ) );				// Attribut title du lien
					$r .= '<a'.($img_title ? ' title="'.$img_title.'"' : '').' href="'.$big_src.'" class="'.$class.'">';
					$r .= $img_from_obj->wp_get_attachment_image($img, $size);
					$r .= '</a>';
				}
				break;

			case 'separate-list':
				// SOUS FORME DE LISTE en deux partie
				$r .= '<div id="complete">';
				foreach($imgs as $k => $img) {
					$r .= '<div class="item" data-item="'.$k.'">';
					$r .= $img_from_obj->wp_get_attachment_image($img, $size);
					$r .= '</div>';
				}
				$r .= '</div><div id="min"><div class="wrap">';
				foreach($imgs as $k => $img) {
					$r .= '<a href="javascript:void(0);" data-cible="'.$k.'">';
					$r .= $img_from_obj->wp_get_attachment_image($img, 'vignette-galerie');
					$r .= '</a>';
				}
				$r .= '</div></div>';
				if(count($imgs)>1)
					$r .= '<button id="prev">▲</button><button id="next">▼</button>';
				break;


			// AJOUTER DES NOUVELLES SORTIES ICI !
			default:
				// DEFAULT
				foreach($imgs as $img) {
					$r .= $img_from_obj->wp_get_attachment_image($img, $size);
				}
				break;
		}
		$r .= $after;
		if($echo) echo $r;
		else return $r;
	}
}

/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  10.FIL D'ARIANE ↓
// (s'apelle via my_bread() dans les templates)
// → cf pdf WordPress SEO par seomix
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
*/
// Récupérer les parents
function myget_category_parents($id, $link = false,$separator = '/',$nicename = false,$visited = array()) {
	$chain = '';
	$parent = &get_category($id);
	if (is_wp_error($parent))return $parent;
	if ($nicename)$name = $parent->name;
	else $name = $parent->cat_name;
	if ($parent->parent && ($parent->parent != $parent->term_id ) && !in_array($parent->parent, $visited)) {
		$visited[] = $parent->parent;$chain .= myget_category_parents(
		$parent->parent, $link, $separator, $nicename, $visited );
	}
	if ($link) $chain .= '<span typeof="v:Breadcrumb"><a href="' . get_category_link($parent->term_id ) . '" title="Voir tous les articles de '.$parent->cat_name.'" rel="v:url" property="v:title">'.$name.'</a></span>' . $separator;
	else $chain .= $name.$separator;
	return $chain;
}

// Construction du fil d'ariane
function mybread() {
	global $wp_query;
	$ped=get_query_var('paged');$rendu = '<div xmlns:v="http://rdf.data-vocabulary.org/#">';
	if ( !is_home() ) {
		$rendu .= '<span id="breadex">Vous &ecirc;tes ici :</span> <span typeof="v:Breadcrumb"><a title="'. get_bloginfo('name') .'" id="breadh" href="'.home_url().'" rel="v:url" property="v:title">'. get_bloginfo('name') .'</a></span>';
	}
	elseif ( is_home() ) {
		$rendu .= '<span id="breadex">Vous &ecirc;tes ici :</span> <span typeof="v:Breadcrumb">Accueil de '. get_bloginfo('name') .'</span>';
	}
	if ( is_category() ) {
		$cat_obj = $wp_query->get_queried_object();
		$thisCat = $cat_obj->term_id;$thisCat = get_category($thisCat);
		$parentCat = get_category($thisCat->parent);
		if ($thisCat->parent != 0) $rendu .= " &raquo; ".myget_category_parents($parentCat, true, " &raquo; ", true);
		if ($thisCat->parent == 0) {
			$rendu .= " &raquo; ";
		}
		if ( $ped <= 1 ) {
			$rendu .= single_cat_title("", false);
		}
		elseif ( $ped > 1 ) {
			$rendu .= '<span typeof="v:Breadcrumb"><a href="' . get_category_link($thisCat ) . '" title="Voir tous les articles de '.single_cat_title("", false).'" rel="v:url" property="v:title">'.single_cat_title("", false).'</a></span>';}}
		elseif ( is_author()){
			global $author;$user_info = get_userdata($author);$rendu .= " &raquo; Articles de l'auteur ".$user_info->display_name."</span>";}
		elseif ( is_tag()){
			$tag=single_tag_title("",FALSE);$rendu .= " &raquo; Articles sur le th&egrave;me <span>".$tag."</span>";}
		elseif ( is_date() ) {
			if ( is_day() ) {
				global $wp_locale;
				$rendu .= '<span typeof="v:Breadcrumb"><a href="'.get_month_link( get_query_var('year'), get_query_var('monthnum') ).'" rel="v:url" property="v:title">'.$wp_locale->get_month( get_query_var('monthnum') ).' '.get_query_var('year').'</a></span> ';
				$rendu .= " &raquo; Archives pour ".get_the_date();
			}
			else if ( is_month() ) {
				$rendu .= " &raquo; Archives pour ".single_month_title(' ',false);
			}
			else if ( is_year() ) {
				$rendu .= " &raquo; Archives pour ".get_query_var('year');
			}
		}
		elseif ( is_archive() && !is_category()){
			$posttype = get_post_type();
			$tata = get_post_type_object( $posttype );
			$var = '';
			$the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
			$titrearchive = $tata->labels->menu_name;
			if (!empty($the_tax)){
				$var = $the_tax->labels->name.' ';
			}
			if (empty($the_tax)){
				$var = $titrearchive;
			}
			$rendu .= ' &raquo; Archives sur "'.$var.'"';
		}
		elseif ( is_search()) {
			$rendu .= " &raquo; R&eacute;sultats de votre recherche <span>&raquo; ".get_search_query()."</span>";
		}
		elseif ( is_404()){
			$rendu .= " &raquo; 404 Page non trouv&eacute;e";
		}
		elseif ( is_single()){
			$category = get_the_category();
			$category_id = get_cat_ID( $category[0]->cat_name );
			if ($category_id != 0) {
				$rendu .= " &raquo; ".myget_category_parents($category_id,TRUE,' &raquo; ')."<span>".the_title('','',FALSE)."</span>";
			}
			elseif ($category_id == 0) {
				$post_type = get_post_type();
				$tata = get_post_type_object( $post_type );
				$titrearchive = $tata->labels->menu_name;
				$urlarchive = get_post_type_archive_link( $post_type );
				$rendu .= ' &raquo; <span typeof="v:Breadcrumb"><a class="breadl" href="'.$urlarchive.'" title="'.$titrearchive.'" rel="v:url" property="v:title">'.$titrearchive.'</a></span> &raquo; <span>'.the_title('','',FALSE).'</span>';
			}
		}
		elseif ( is_page()) {
			$post = $wp_query->get_queried_object();
			if ( $post->post_parent == 0 ){
				$rendu .= " &raquo; ".the_title('','',FALSE)."";
			}
			elseif ( $post->post_parent != 0 ) {
				$title = the_title('','',FALSE);$ancestors = array_reverse(get_post_ancestors($post->ID));array_push($ancestors, $post->ID);
				foreach ( $ancestors as $ancestor ){
					if( $ancestor != end($ancestors) ){
						$rendu .= '&raquo; <span typeof="v:Breadcrumb"><a href="'. get_permalink($ancestor) .'" rel="v:url" property="v:title">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</a></span>';
					}else {
						$rendu .= ' &raquo; '.strip_tags(apply_filters('single_post_title',get_the_title($ancestor))).'';
					}
				}
			}
		}

	if ( $ped >= 1 ) {
		$rendu .= ' (Page '.$ped.')';
	}
	$rendu .= '</div>';
	echo $rendu;
}

