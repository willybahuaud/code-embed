<?php 
if( $c = ( $d = get_post_meta( $post->ID, '_liaison_slideshow', true ) ) ? $d : get_option( 'carousel' ) ) {
	// if( is_post_type_archive( 'produit' ) || is_tax( array( 'sauce', 'tendance', 'conditionnement', 'application' ) ) )
	// 	$c = ( $c_arch = get_option( 'carousel_produits' ) ) ? $c_arch : $c;
	// elseif( is_post_type_archive( 'presse' ) || is_singular( 'presse' ) )
	// 	$c = ( $c_arch = get_option( 'carousel_presse' ) ) ? $c_arch : $c;
	elseif( is_home() || is_single() )
		$c = ( $c_arch = get_option( 'carousel_actualites' ) ) ? $c_arch : $c;

	$carousel = get_images_src( 'carousel-light', false, $c );
	echo '<section id="carousel-article" class="carousel-article">';
	foreach( $carousel as $image )
		echo '<img src="' . $image[ 0 ] . '" width="' . $image[ 1 ] . '" height="' . $image[ 2 ] . '" alt="">';
	echo '<div class="cycle-pager"></div>';
	echo '</section>';
}
