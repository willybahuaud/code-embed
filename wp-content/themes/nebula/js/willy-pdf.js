jQuery(document).ready(function($) {
	var formfield = null;
	$('.upload_pdf_button').click(function() {
		$('html').addClass('pdf');
		var c = $(this).attr('data-cible');
		formfield = $('.url_pdf_input[data-input="'+c+'"]');
		tb_show('', 'media-upload.php?type=file&TB_iframe=true');
		return false;
	});
	
	// user inserts file into post. only run custom if user started process using the above process
	// window.send_to_editor(html) is how wp would normally handle the received data
	window.original_send_to_editor = window.send_to_editor;
	window.send_to_editor = function(html){
		// console.log(html);
	    var fileurl;
		if (formfield != null) {
			fileurl = $(html).filter('a').attr('href');
			formfield.val(fileurl);
			tb_remove();
			$('html').removeClass('pdf');
			formfield = null;
		} else {
			window.original_send_to_editor(html);
		}
	};
});