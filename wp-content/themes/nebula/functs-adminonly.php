<?php
/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 2. MISE A DISPOSITION DE NOUVELLES TAILLES D'IMAGE DANS L'EDITEUR ↓
// → article screen feed  (allégé au lait de ponay ^^)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
add_filter( 'image_size_names_choose', 'sf_image_size_names_choose' );
function sf_image_size_names_choose($size_names) {
	// $size_names['mini-image'] = __('Petite image','fcx');		// POSSIBILITE DE RENOMMER UNE IMAGE
	global $_wp_additional_image_sizes;
	if ( count($_wp_additional_image_sizes) ) {
		foreach($_wp_additional_image_sizes as $name => $props) {
			$size_names[$name] = __($name,'fcx');
		}
	}
	return $size_names;
}
