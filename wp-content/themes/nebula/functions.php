<?php
/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 2. FORMULAIRE DE CONTACT ↓
// en lien avec l'option de thème usermail
// s'éxécute dans le back, mais à partir du front...
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

//chopper email utilisateur
function get_user_mail($userid){
	$u = get_userdata( $userid );
	return $u->user_email; 
}
//chopper joli nom utilisateur
function get_user_name($userid){
	$u = get_userdata( $userid );
	return $u->user_nicename; 
}
 
//envoi formulaire
add_action("wp_ajax_nopriv_sendform", "sendform_contact");
add_action("wp_ajax_sendform", "sendform_contact");
function sendform_contact(){
	$destinataire = get_user_mail(get_option('fcx_usermail'));
	$nom_destinataire =  get_user_name(get_option('fcx_usermail'));
	if((isset($_POST['nom']))&&(isset($_POST['email']))&&(isset($_POST['message']))) :
		$erreur;
		$message;
		$send;
		if(($_POST['nom']=='Nom')||($_POST['nom']=='')){
			$send = 0;
			$erreur['type1'] = 'retournom';
		}
		if(!is_email(urldecode($_POST['email']))){
			$send = 0;
			$erreur['type3'] = 'retourmail';
		}
		if(($_POST['message']=='Votre message')||$_POST['message']==''){
			$send = 0;
			$erreur['type4'] = 'retourmessage';
		}
		if($_POST['prenom']!=''){
			$send = 0;
			$erreur['type5'] = 'robot';
		}
		if(count($erreur)==0){
			$to = addslashes($_POST['email']);				
			$subject = __('Nouveau message pour ','fcx').get_bloginfo('url');
			$contenu = addslashes($_POST['message']);
			$quic = utf8_decode($_POST['nom']) ."\r\n". urldecode($_POST['email'])."\r\n".utf8_decode($_POST['adresse']) ."\r\n". urldecode($_POST['tel']) ."\r\n \r\n message: \r\n";
			
			$headers = 'From: "'.$nom_destinataire.'"<'.$destinataire.'>' . "\r\n" .
			'Reply-To: "'.$nom_destinataire.'"<'.$destinataire.'>' . "\r\n" .
			'Return-Path: '.$destinataire."\r\n" .
			'X-Mailer: PHP/' . phpversion();
			
			$headers2 = 'From: "'.$_POST['nom'].'"<'.urldecode($_POST['email']).'>' . "\r\n" .
			'Reply-To: "'.$_POST['nom'].'"<'.urldecode($_POST['email']).'>' . "\r\n" .
			'Return-Path: '.urldecode($_POST['email'])."\r\n" .
			'X-Mailer: PHP/' . phpversion();
			
			$send = 1;
			mail ( $destinataire , $subject , urldecode($quic).stripslashes(utf8_decode($contenu)), $headers2);
			mail ( $to, 'votre message à été transmis au '.get_bloginfo('name').', nous y répondrons dans les plus brefs délais' , 'Votre message :'."\r \n". stripslashes(utf8_decode($contenu)), $headers);
		}
		$arr = array('send'=>$send,'erreur' => $erreur);
		echo json_encode($arr);
	endif;
	exit;
}

add_theme_support('root-relative-urls');
add_theme_support('rewrites');
add_theme_support('bootstrap-gallery');
add_theme_support('nice-search');

/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 4. LANGUES ↓
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
// ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
load_theme_textdomain( 'fcx', TEMPLATEPATH.'/languages' );
	
/**
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ 
// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ON APPELLE LES FUNCTIONS SPÉCIFIQUES À L'ADMIN OU AU FRONT OFFICE ↓
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ 
// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★ 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
if(is_admin())
	// POUR LE BACK OFFICE
	get_template_part( 'functs', 'adminonly' );
else
	// POUR LE FRONT OFFICE
	get_template_part( 'functs', 'frontendonly' );

