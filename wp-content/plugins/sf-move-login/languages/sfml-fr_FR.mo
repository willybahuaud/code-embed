��    
      l      �       �   �   �   6   {  1   �  �   �  l   �  s   (  2   �  %   �     �  �    �   �  5   �  F   �  #    |   1  �   �  9   4	  =   n	     �	                  
         	                  <i>SF Move Login</i> needs access to the %1$s file. Please visit the %2$s settings page and copy/paste the given code into the %1$s file. <strong>SF Move Login</strong> has not been activated. Change your login url to http://example.com/login If the plugin fails to add the new rewrite rules to your %1$s file on activation, add the following to your %1$s file in %2$s, replacing other %3$s rules if they exist, <strong>above</strong> the line reading %4$s: It seems the url rewrite module is not activated on your server. <i>SF Move Login</i> will not be activated. It seems your server configuration prevent the plugin to work properly. <i>SF Move Login</i> will not be activated. Make sure to enable permalinks for ALL your blogs. No no no, the login form is not here. Please make sure to enable %s. Project-Id-Version: sfml 0.2
Report-msgid -Bugs-To: http://scri.in/contact/
POT-Creation-Date: 2013-06-01 00:04+0100
PO-Revision-Date: 2013-09-08 21:49+0100
Last-Translator: Grégory Viguier <gregory@screenfeed.fr>
Language-Team: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e
Language: fr_FR
X-Generator: Poedit 1.5.7
 <i>SF Move Login</i> a besoin d'accéder au fichier %1$s. Merci de vous rendre sur la page de réglages des %2$s et de copier/coller le code fourni dans le fichier %1$s. <strong>SF Move Login</strong> n'a pas été activé. Changez l'url de votre page de connexion pour http://example.com/login Si l'extension ne peut ajouter les nouvelles règles de réécriture à votre fichier %1$s à son activation, ajoutez les lignes suivantes à votre fichier %1$s dans %2$s, en remplacement des autres règles liées à %3$s si elles existent, <strong>au-dessus</strong> de la ligne %4$s&#160;: Il semble que le module de réécriture d'url n'est pas activé sur votre serveur. <i>SF Move Login</i> ne sera pas activé. Il semble que votre configuration serveur empêche l'extension de fonctionner correctement. <i>SF Move Login</i> ne sera pas activé. Assurez-vous d'activer les permaliens sur TOUS vos blogs. Non non non, le formulaire de connexion ne se trouve pas ici. Assurez-vous d'activer les %s. 