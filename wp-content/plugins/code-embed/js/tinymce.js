jQuery(document).ready(function($) {
	tinymce.create('tinymce.plugins.pcb', {
		init : function(ed, url) {
			//Switch Sources
			$(document).on('change', '#pastacode-service', function( e ){
				var serv = $(this).val();
				if(['github','gist','bitbucket','pastebin','file'].contains(serv)){
				console.log(serv);
					$('.pastacode-args').hide().find('input').val('');
					$('.pastacode-args.'+serv).show();
				}
			} );
			//Insert
			$(document).on( 'click','#pastacode-insert', function( e ) {
				e.preventDefault();
				
				ed.execCommand(
					'mceInsertContent',
					false,
					pastacode_create_shortcode()
				);
				
				tb_remove();
			} );
			ed.addButton('pcb', {
				title : 'Pastacode',
				image : url+'/../images/pastacode_logo.png',
				onclick : function() {
					tb_show('Pastacode', ajaxurl+'?action=pastacode_shortcode_printer&width=600&height=700');
				}
			});
		},
	});
	tinymce.PluginManager.add('pcb', tinymce.plugins.pcb);
});

function pastacode_create_shortcode() {
	var inputs = jQuery('#pastacode-shortcode-gen').serializeArray();
	var shortcode = ' [pastacode ';
	for( var a in inputs ) {
		if( inputs[a].value == "" )
			continue;
			
		inputs[a].name = inputs[a].name.replace( 'pastacode-', '' );
		shortcode += ' '+inputs[a].name+'="'+inputs[a].value+'"';
	}
	
	shortcode += ' ] ';
	
	return shortcode;
}

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}