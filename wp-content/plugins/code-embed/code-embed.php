<?php 
/*
Plugin Name: code embed
Plugin URI: http://wordpress.org/extend/plugins/code-embed/
Description: Embed GitHub, Gist or Bitbucket files.
<<<<<<< HEAD
Version: 0.9
Author: Willy Bahuaud, Julio Potier
=======
Version: 1.0
Author: Willy Bahuaud
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
Author URI: http://wabeo.fr
License: cc-by 2.0
*/

<<<<<<< HEAD
add_shortcode( 'pastacode', 'pastacode_embed' );
function pastacode_embed( $atts ) {
    extract( shortcode_atts( array(
        'type'          => false,
        'user'          => false,
        'path'          => false,
        'repos'         => false,
        'revision'      => 'master',
        'lines'         => false,
        'lang'          => 'markup',
        'highlight'     => false,
        'message'       => '',
        'linenumbers'   => false,
        'showinvisible' => false
        ), $atts, 'pastacode' ) );
=======
define( 'CE_VERSION', '1.0' );
add_shortcode( 'code-embed', 'wabeo_code_embed' );
function wabeo_code_embed( $atts, $content ) {
    extract( shortcode_atts( array(
        'type'     => false,
        'user'     => false,
        'path'     => false,
        'repos'    => false,
        'revision' => 'master',
        'lines'    => false,
        'lang'     => 'markup',
        'highlight' => false,
        'message'   => ''
        'linenumbers'   => false,
        'showinvisible' => false
        ), $atts, 'code-embed' ) );
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00

    if( empty( $type ) && !empty( $content ) )
        $type = $content;

    $code_embed_transient = 'code_embed_' . substr( md5( serialize( $atts ) ), 0, 14 );

    if( false === ( $source = get_transient( $code_embed_transient ) ) ) {

        $source = array();


        switch( $type ){
            case 'github' :
                if( $user && $repos && $path ) {
                    $req  = wp_sprintf( 'https://api.github.com/repos/%s/%s/contents/%s', $user, $repos, $path );
                    $code = wp_remote_get( $req );
                    if( ! is_wp_error( $code ) && 200 == wp_remote_retrieve_response_code( $code ) ) {
                        $data = json_decode( wp_remote_retrieve_body( $code ) );
                        $source[ 'name' ] = $data->name;
                        $source[ 'code' ] = esc_html( base64_decode( $data->content ) );
                        $source[ 'url' ]  = $data->html_url;
                        $source[ 'raw' ]  = wp_sprintf( 'https://raw.github.com/%s/%s/%s/%s', $user, $repos, $revision, $path );
                    }
                }
                break;
            case 'gist' :
                if( $path ) {
                    $req  = wp_sprintf( 'https://api.github.com/gists/%s', $path );
                    $code = wp_remote_get( $req );
                    if( ! is_wp_error( $code ) && 200 == wp_remote_retrieve_response_code( $code ) ) {
                        // die(var_dump( $code));
                        $data = json_decode( wp_remote_retrieve_body( $code ) );
                        $source[ 'url' ]  = $data->html_url;
                        $data = (array)$data->files;
                        $data = reset($data);
                        $source[ 'name' ] = $data->filename;
                        $source[ 'code' ] = esc_html( $data->content );                 
                        $source[ 'raw' ]  = $data->raw_url;
                    }
                }
                break;
            case 'bitbucket' :
                if( $user && $repos && $path ) {
                    $req  = wp_sprintf( 'https://bitbucket.org/api/1.0/repositories/%s/%s/raw/%s/%s', $user, $repos, $revision, $path );

                    $code = wp_remote_get( $req );
                    if( ! is_wp_error( $code ) && 200 == wp_remote_retrieve_response_code( $code ) ) {
                        $source[ 'name' ] = basename( $path );
                        $source[ 'code' ] = esc_html( wp_remote_retrieve_body( $code ) );
                        $source[ 'url' ]  = wp_sprintf( 'https://bitbucket.org/%s/%s/src/%s/%s', $user, $repos, $revision, $path );
                        $source[ 'raw' ]  = $req;
                    }
                }
                break;
            case 'file' :
                if( $path ) {
                    $upload_dir = wp_upload_dir();
                    $path = str_replace( '../', '', $path );
                    $req  = esc_url( trailingslashit( $upload_dir[ 'baseurl' ] ) . $path );
                    $code = wp_remote_get( $req );
                    if( ! is_wp_error( $code ) && 200 == wp_remote_retrieve_response_code( $code ) ) {
                        $source[ 'name' ] = basename( $path );
                        $source[ 'code' ] = esc_html( wp_remote_retrieve_body( $code ) );
                        $source[ 'url' ]  = ( $req );
                    }
                }
                break;            
            case 'pastebin' :
                if( $path ) {
                    $req  = wp_sprintf( 'http://pastebin.com/raw.php?i=%s', $path );
                    $code = wp_remote_get( $req );
                    if( ! is_wp_error( $code ) && 200 == wp_remote_retrieve_response_code( $code ) ) {
                        $source[ 'name' ] = $path;
                        $source[ 'code' ] = esc_html( wp_remote_retrieve_body( $code ) );
                        $source[ 'url' ]  = wp_sprintf( 'http://pastebin.com/%s', $path );
                        $source[ 'raw' ]  = wp_sprintf( 'http://pastebin.com/raw.php?i=%s', $path );
                    }
                }
                break;
            default : 
                if( !empty( $content ) ){
                        $source[ 'code' ] = esc_html( str_replace( '<br />', '', $content ) );
                }elseif( ! empty( $message ) ){
                    return '<span class="wabeo_ce_message">' . esc_html( $message ) . '</span>';
                }
                break;
        }

        if( ! empty( $source[ 'code' ] ) ) {
            //Wrap lines
            if( $lines ) {
                $lines = array_map( 'intval', explode( ',', $lines ) );
                $source[ 'code' ] = implode( "\n", array_slice( preg_split( '/\r\n|\r|\n/', $source[ 'code' ] ), $lines[0]-1, ( $lines[1] - $lines[0] ) + 1 ) );
            }

            set_transient( $code_embed_transient, $source, get_option( 'wabeo_ce_cache_duration', DAY_IN_SECONDS * 7 ) );
        }
    }
    if( ! empty( $source[ 'code' ] ) ) {

        //Load scripts
        wp_enqueue_style( 'prismcss' );
        wp_enqueue_script( 'prismjs' ); 

<<<<<<< HEAD
        if( true == $linenumbers ) {
            wp_enqueue_style( 'prism-linenumbercss' );
            wp_enqueue_script( 'prism-linenumber' );
        }
        if( true == $showinvisible ) {        
            wp_enqueue_style( 'prism-show-invisiblecss' );
            wp_enqueue_script( 'prism-show-invisible' );  
        }

=======
        if( true == get_option( 'wabeo_ce_linenumbers', false ) ) {
            wp_enqueue_style( 'prism-linenumbercss' );
            wp_enqueue_script( 'prism-linenumber' );
        }
        if( true == get_option( 'wabeo_ce_showinvisible', false ) ) {
            wp_enqueue_style( 'prism-show-invisiblecss' );
            wp_enqueue_script( 'prism-show-invisible' );  
        }
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
        //highlight
        if( preg_match( '/([0-9-,]+)/', $highlight ) ) {
            $highlight_val = ' data-line="' . $highlight . '"';
            wp_enqueue_script( 'prism-highlight' );
            wp_enqueue_style( 'prism-highlighcss' );
        } else {
            $highlight_val = '';
        }

        //Wrap
        $output = array();
<<<<<<< HEAD
        $output[] = '<div class="code-embed-wrapper">';
=======

>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
        $output[] = '<pre class="language-' . sanitize_html_class( $lang ) . ' code-embed-pre line-numbers" ' . $highlight_val . '><code class="language-' . sanitize_html_class( $lang ) . ' code-embed-code">'
        . $source[ 'code' ] .
        '</code></pre>';
        $output[] = '<div class="code-embed-infos">';
        if( isset( $source[ 'url' ] ) )
            $output[] = '<a href="' . esc_url( $source[ 'url' ] ) . '" title="' . sprintf( esc_attr__( 'See %s', 'wabeo' ), $source[ 'name' ] ) . '" target="_blank" class="code-embed-name">' . esc_html( $source[ 'name' ] ) . '</a>';
        if( isset( $source[ 'raw' ] ) )
            $output[] = '<a href="' . esc_url( $source[ 'raw' ] ) . '" title="' . sprintf( esc_attr__( 'Back to %s' ), $source[ 'name' ] ) . '" class="code-embed-raw" target="_blank">' . __( 'view raw', 'wabeo' ) . '</a>';
        $output[] = '</div>';
        $output[] = '</div>';
        $output = implode( "\n", $output );

        return $output;
    } elseif( ! empty( $message ) ) {
        return '<span class="wabeo_ce_message">' . esc_html( $message ) . '</span>';
    }
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wabeo_settings_action_links', 10, 2 );
function wabeo_settings_action_links( $links, $file )
{
    if( current_user_can( 'manage_options' ) )
        array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=code_embed' ) . '">' . __( 'Settings' ) . '</a>' );
    return $links;
}

//Register scripts
add_action( 'wp_enqueue_scripts', 'wabeo_enqueue_prismjs' );
function wabeo_enqueue_prismjs() {
<<<<<<< HEAD
    wp_register_script( 'prismjs', plugins_url( '/js/prism.js', __FILE__ ), false, '0.9', true );
    wp_register_script( 'prism-highlight', plugins_url( '/plugins/line-highlight/prism-line-highlight.min.js', __FILE__ ), array( 'prismjs' ), '0.9', true );
    wp_register_script( 'prism-linenumber', plugins_url( '/plugins/line-numbers/prism-line-numbers.min.js', __FILE__ ), array( 'prismjs' ), '0.9', true );
    wp_register_script( 'prism-show-invisible', plugins_url( '/plugins/show-invisibles/prism-show-invisibles.min.js', __FILE__ ), array( 'prismjs' ), '0.9', true );
    wp_register_style( 'prismcss', plugins_url( '/css/' . apply_filters( 'prism_css', get_option( 'wabeo_ce_style', 'prism-willy2' ) ) . '.css', __FILE__ ), false, '0.9', 'all' );      
    wp_register_style( 'prism-highlighcss', plugins_url( '/plugins/line-highlight/prism-line-highlight.css', __FILE__ ), false, '0.9', 'all' );      
    wp_register_style( 'prism-linenumbercss', plugins_url( '/plugins/line-numbers/prism-line-numbers.css', __FILE__ ), false, '0.9', 'all' );  
    wp_register_style( 'prism-show-invisiblecss', plugins_url( '/plugins/show-invisibles/prism-show-invisibles.css', __FILE__ ), false, '0.9', 'all' );      
=======
    wp_register_script( 'prismjs', plugins_url( '/js/prism.js', __FILE__ ), false, CE_VERSION, true );
    wp_register_script( 'prism-highlight', plugins_url( '/plugins/line-highlight/prism-line-highlight.min.js', __FILE__ ), array( 'prism' ), CE_VERSION, true );
    wp_register_script( 'prism-linenumber', plugins_url( '/plugins/line-numbers/prism-line-numbers.min.js', __FILE__ ), array( 'prismjs' ), CE_VERSION, true );
    wp_register_script( 'prism-show-invisible', plugins_url( '/plugins/show-invisibles/prism-show-invisibles.min.js', __FILE__ ), array( 'prismjs' ), CE_VERSION, true );
    wp_register_style( 'prismcss', plugins_url( '/css/' . apply_filters( 'prism_css', get_option( 'wabeo_ce_style', 'prism-willy2' ) ) . '.css', __FILE__ ), false, CE_VERSION, 'all' );      
    wp_register_style( 'prism-highlighcss', plugins_url( '/plugins/line-highlight/prism-line-highlight.css', __FILE__ ), false, CE_VERSION, 'all' );      
    wp_register_style( 'prism-linenumbercss', plugins_url( '/plugins/line-numbers/prism-line-numbers.css', __FILE__ ), false, CE_VERSION, 'all' );  
    wp_register_style( 'prism-show-invisiblecss', plugins_url( '/plugins/show-invisibles/prism-show-invisibles.css', __FILE__ ), false, CE_VERSION, 'all' );      
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
    
}

function drop_wge_transients( $param ) {
    global $wpdb;
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '_transient_code_embed_%'" );
    return $param;
}

/**
//Admin Settings
*/
add_action('admin_menu', 'wabeo_code_embed_create_menu');
function wabeo_code_embed_create_menu() {
    add_options_page( __( 'Code Embed Settings', 'wabeo' ), __( 'Code Embed', 'wabeo' ), 'manage_options', 'code_embed', 'code_embed_settings_page' );
    register_setting( 'code_embed', 'wabeo_ce_cache_duration' );
    register_setting( 'code_embed', 'wabeo_ce_style' );
    register_setting( 'code_embed', 'wabeo_ce_linenumbers' );
    register_setting( 'code_embed', 'wabeo_ce_showinvisible' );
}

function wabeo_ce_setting_callback_function( $args ) {
    extract( $args );

    $value_old = get_option( $name );
    
    echo '<select name="' . $name . '" id="' . $name . '">';
    foreach( $options as $key => $option )
        echo '<option value="' . $key . '" ' . selected( $value_old==$key, true, false ) . '>' . esc_html( $option ) . '</option>';
    echo '</select>';
}

add_filter( 'pre_update_option_wabeo_ce_cache_duration', 'drop_wge_transients' );

function code_embed_settings_page() {
?>
<div class="wrap">
    <?php screen_icon(); ?>
<h2><?php _e( 'Code Embed', 'wabeo' ); ?></h2>

<?php 

    add_settings_section( 'wabeo_ce_setting_section',
        __( 'General Settings', 'wabeo' ),
        '__return_false',
        'code_embed' );

    add_settings_field( 'wabeo_ce_cache_duration',
        __( 'Caching duration', 'wabeo' ),
        'wabeo_ce_setting_callback_function',
        'code_embed',
        'wabeo_ce_setting_section',
        array(
            'options' => array(
                HOUR_IN_SECONDS     => sprintf( __( '%s hour' ), '1' ),
                HOUR_IN_SECONDS * 12 => __( 'Twice Daily' ),
                DAY_IN_SECONDS      => __( 'Once Daily' ),
                DAY_IN_SECONDS * 7     => __( 'Once Weekly', 'wabeo' ),
                0      => __( 'Never', 'wabeo' ),
                ),
            'name' => 'wabeo_ce_cache_duration'
         ) );

    add_settings_field( 'wabeo_ce_style',
        __( 'Syntax Coloration Style', 'wabeo' ),
        'wabeo_ce_setting_callback_function',
        'code_embed',
        'wabeo_ce_setting_section',
        array(
            'options' => array(
                'prism'          => __( 'Prism', 'wabeo' ),
                'prism-dark'     => __( 'Dark', 'wabeo' ),
                'prism-funky'    => __( 'Funky', 'wabeo' ),
                'prism-coy'      => __( 'Coy', 'wabeo' ),
                'prism-okaidia'  => __( 'Okaïdia', 'wabeo' ),
                'prism-tomorrow' => __( 'Tomorrow', 'wabeo' ),
                'prism-twilight' => __( 'Twilight', 'wabeo' ),
                ),
            'name' => 'wabeo_ce_style'
         ) );

    add_settings_field( 'wabeo_ce_linenumbers',
        __( 'Show line numbers', 'wabeo' ),
        'wabeo_ce_setting_callback_function',
        'code_embed',
        'wabeo_ce_setting_section',
        array(
            'options' => array(
                'y'          => __( 'Yes', 'wabeo' ),
                'n'     => __( 'No', 'wabeo' ),
                ),
            'name' => 'wabeo_ce_linenumbers'
         ) );

    add_settings_field( 'wabeo_ce_showinvisible',
        __( 'Show invisible chars', 'wabeo' ),
        'wabeo_ce_setting_callback_function',
        'code_embed',
        'wabeo_ce_setting_section',
        array(
            'options' => array(
                'y'          => __( 'Yes', 'wabeo' ),
                'n'     => __( 'No', 'wabeo' ),
                ),
            'name' => 'wabeo_ce_showinvisible'
         ) );

 ?>
<form method="post" action="options.php">
    <?php 

    settings_fields( 'code_embed' );
    do_settings_sections( 'code_embed' );
    submit_button();

    ?>

</form>
</div>
<?php }


register_activation_hook( __FILE__, 'code_embed_activation' );
function code_embed_activation()
{
<<<<<<< HEAD
    add_option( 'wabeo_ce_cache_duration', DAY_IN_SECONDS * 7 );
=======
        add_option( 'wabeo_ce_cache_duration', DAY_IN_SECONDS * 7 );
        add_option( 'wabeo_ce_style', 'prism' );
        add_option( 'wabeo_ce_showinvisible', 'n' );
        add_option( 'wabeo_ce_linenumbers', 'n' );
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
}

register_uninstall_hook( __FILE__, 'code_embed_uninstaller' );
function code_embed_uninstaller()
{
<<<<<<< HEAD
    delete_option( 'wabeo_ce_cache_duration' );
}


/**
Add button to tinymce
*/
//Button
add_action( 'admin_init', 'pastacode_button_editor' );
function pastacode_button_editor() {

    // Don't bother doing this stuff if the current user lacks permissions
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
        return false;

    if ( get_user_option('rich_editing') == 'true') {
        add_filter( 'mce_external_plugins', 'pastacode_script_tiny' );
        add_filter( 'mce_buttons', 'pastacode_register_button' );
    }
}

function pastacode_register_button($buttons) {
    array_push($buttons, "|", "pcb");
    return $buttons;
}
function pastacode_script_tiny($plugin_array) {
    $plugin_array['pcb'] = plugins_url( '/js/tinymce.js', __FILE__ );
    return $plugin_array;
}

add_action( 'wp_ajax_pastacode_shortcode_printer', 'wp_ajax_pastacode_box' );
function wp_ajax_pastacode_box(){
    global $ips_options, $wp_styles;
    if ( !empty($wp_styles->concat) ) {
        $dir = $wp_styles->text_direction;
        $ver = md5("$wp_styles->concat_version{$dir}");

        // Make the href for the style of box
        $href = $wp_styles->base_url . "/wp-admin/load-styles.php?c={$zip}&dir={$dir}&load=media&ver=$ver";
        echo "<link rel='stylesheet' href='" . esc_attr( $href ) . "' type='text/css' media='all' />\n";
    }

    ?>
    <h3 class="media-title"><?php _e('Paste a code', 'wabeo'); ?></h3>

    <form name="pastacode-shortcode-gen" id="pastacode-shortcode-gen">
        <div id="media-items">
            <div class="media-item media-blank">

                <table class="describe" style="width:100%;margin-top:1em;"><tbody>

                    <tr valign="top" class="field">
                        <th class="label" scope="row"><label for="pastacode-service"><?php _e('Select a service', 'wabeo'); ?></th>
                        <td>
                            <select name="pastacode-type" id="pastacode-service">
                                <?php
                                $types  = array(
                                    '0'         => __( 'Select a provider', 'pastacode' ),
                                    'github'    => 'Github',
                                    'gist'      => 'Gist',
                                    'bitbucket' => 'Bitbucket',
                                    'pastebin'  => 'Pastebin',
                                    'file'      => 'File',
                                );
                                foreach( $types as $k => $type )
                                    echo '<option value="' . $k . '">' . $type . '</value>';
                                    ?>
                            </select>
                        </td>
                    </tr>

                    <tr valign="top" class="field">
                        <th class="label" scope="row"><label for="pastacode-lang"><?php _e('Select a service', 'wabeo'); ?></th>
                        <td>
                            <select name="pastacode-lang" id="pastacode-lang">
                                <?php
                                $types  = array(
                                    'markup'       => 'HTML',
                                    'css'          => 'CSS',
                                    'javascript'   => 'JavaScript',
                                    'php'          => 'PHP',
                                    'c'            => 'C',
                                    'c++'          => 'C++',
                                    'java'         => 'Java',
                                    'sass'         => 'Sass',
                                    'python'       => 'Python',
                                    'sql'          => 'SQL',
                                    'ruby'         => 'Ruby',
                                    'coffeescript' => 'CoffeeScript',
                                    'bash'         => 'Bash',
                                );
                                foreach( $types as $k => $type )
                                    echo '<option value="' . $k . '">' . $type . '</value>';
                                    ?>
                            </select>
                        </td>
                    </tr>

                    <tr valign="top" class="field pastacode-args github bitbucket">
                        <th class="label" scope="row"><label for="pastacode-username"><span class="alignleft"><?php _e('User of repository', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-user" id="pastacode-username" placeholder="<?php _e( 'User name', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field pastacode-args github bitbucket">
                        <th class="label" scope="row"><label for="pastacode-repository"><span class="alignleft"><?php _e('Repository', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-repos" id="pastacode-repository" placeholder="<?php _e( 'Repository slug', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field pastacode-args gist pastebin">
                        <th class="label" scope="row"><label for="pastacode-path"><span class="alignleft"><?php _e('Code ID', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-path" id="pastacode-path" placeholder="<?php _e( 'ID', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field pastacode-args github bitbucket file">
                        <th class="label" scope="row"><label for="pastacode-path"><span class="alignleft"><?php _e('File path inside the repository', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-path" id="pastacode-path" placeholder="<?php _e( 'File path', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field pastacode-args github bitbucket">
                        <th class="label" scope="row"><label for="pastacode-revision"><span class="alignleft"><?php _e('Version number', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-revision" id="pastacode-revision" placeholder="<?php _e( 'master', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field">
                        <th class="label" scope="row"><label for="pastacode-lines"><span class="alignleft"><?php _e('Visibles lines', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-lines" id="pastacode-lines" placeholder="<?php _e( '1-20', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field">
                        <th class="label" scope="row"><label for="pastacode-highlight"><span class="alignleft"><?php _e('Highlited lines', 'wabeo'); ?></span></label></th>
                        <td>
                            <input type="text" name="pastacode-highlight" id="pastacode-highlight" placeholder="<?php _e( '1,2,5-6', 'wabeo' ); ?>"/>
                        </td>
                    </tr>

                    <tr valign="top" class="field">
                        <td>
                            <p style="text-align:center;"><input name="pastacode-insert" type="submit" class="button-primary" id="pastacode-insert" tabindex="5" accesskey="p" value="<?php _e('Insert shortcode', 'wabeo') ?>"></p>
                        </td>
                    </tr>

                </tbody></table>
            </div>
        </div>

    </form>
    <?php die();
}

=======
        delete_option( 'wabeo_ce_cache_duration' );
        delete_option( 'wabeo_ce_style' );
>>>>>>> f821cb121af60c44a96f97dea552ee37c7586e00
