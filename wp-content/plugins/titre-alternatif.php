<?php

/*
Plugin Name: Titres alternatifs
Description: This plugin allow to enter a secondary title to public post types.
Version:0.9.2
Author: Willy Bahuaud
Author URI: http://wabeo.fr
*/
/**
METABOX
*/
function ta_titre() {
    $pt = get_post_types( array( 'public' => true ) );
    foreach( $pt as $p )
        add_meta_box( 'titre_alternatif', 'Titre court', 'titre_alternatif', $p, 'side', 'default' );
}
add_action( 'add_meta_boxes', 'ta_titre' );

function titre_alternatif( $post ) {
    wp_nonce_field('update-ta_title_'.$post->ID, '_wpnonce_ta_title');
    echo '<input type="text" name="ta_title" style="width:100%;" value="' . ( false !== ( $titre = get_post_meta( $post->ID, '_titre_alternatif', true ) ) ? esc_attr( $titre ) : '' ) . '" placeholder="Titre alternatif">';
}

function ta_save_title( $id ) {
    if( ( !defined( 'DOING_AJAX' ) || !DOING_AJAX ) && isset( $_POST[ 'ta_title' ] ) ) {
        check_admin_referer( 'update-ta_title_' . $_POST[ 'post_ID' ], '_wpnonce_ta_title' );
        update_post_meta( $id, '_titre_alternatif', $_POST[ 'ta_title' ] );
    }
}
add_action( 'save_post', 'ta_save_title' );

/**
AJOUT DE CHAMP À LA LISTE DES ARTICLES
*/
function ta_titre_columns($defaults) {
    unset($defaults['author']);
    unset($defaults['tags']);

    $new = array();
    foreach( $defaults as $k => $e ) {
        $new[ $k ] = $e;
        if( $k == 'title' )
            $new[ 'titre_alternatif' ] = 'Titre alternatif';
    }
    
    return $new;
}
add_filter('manage_posts_columns', 'ta_titre_columns');
add_filter('manage_pages_columns', 'ta_titre_columns');

function show_titre_columns($name){
    global $post;
    $mypost = $post->ID;
    switch ($name){
        case 'titre_alternatif':
            echo ($titre = get_post_meta( $mypost, '_titre_alternatif', true ) ) ? '<strong>' . esc_html( $titre ) . '<strong>' : '<i>Non renseigné</i>';
            break;
    }
}
add_action('manage_posts_custom_column',  'show_titre_columns');
add_action('manage_pages_custom_column',  'show_titre_columns');

//suport post types
function custom_column_4_cpts() {
    $pt = get_post_types( array( 'public' => true, '_builtin' => false ) );
    foreach( $pt as $p ) {
        add_action('manage_' . $p . '_posts_column',  'ta_titre_columns');
        add_action('manage_' . $p . '_posts_custom_column',  'show_titre_columns');
    }
}
// add_action( 'admin_init', 'custom_column_4_cpts' );

function ta_print_css() {
    echo '<style>.fixed .column-titre_alternatif{width:15em;}</style>';
}
add_action('admin_head','ta_print_css');

/**
REPLACE TITLE BY TITLE ALTERNATIVE IN NAV
*/
function ta_replace_title( $menu_item ) {
    if( isset( $menu_item->type ) && $menu_item->type == 'post_type' ) {
        $titre_alternatif = get_post_meta( $menu_item->object_id, '_titre_alternatif', true );

        $original_object = get_post( $menu_item->object_id );
        $original_title = $original_object->post_title;

        $menu_item->title = ( ( $menu_item->post_title == $original_title ) && ( ! is_null($titre_alternatif) && $titre_alternatif != '' ) ) ? $titre_alternatif  : $menu_item->title;
    }
    return $menu_item;
}
add_filter( 'wp_setup_nav_menu_item', 'ta_replace_title' );